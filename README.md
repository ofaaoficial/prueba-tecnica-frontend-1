# Prueba Técnica
Solución de la prueba técnica.
## Rutas web
| Función | URL | Seguridad |
| :---: | :---: |  :---: |
| Ingreso | `/` |  No |
| Registro | `/registro` | No |
| Noticias | `/noticias` | Si |

**URL en Github Pages: https://ofaaoficial.github.io/prueba-tecnica-frontend/**

## Imagenes
### Ingresar
![Ingreso](imgs/ingresar.png)

### Registro
![Registro](imgs/registro.png)

### Noticias
![Noticias](imgs/noticias.png)

## Licencia 🔥
Copyright © 2020-present [Oscar Amado](https://github.com/ofaaoficial) 🧔
