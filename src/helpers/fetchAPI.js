const PATH_URL = 'https://backend-prueba-tecnica.herokuapp.com';

export const ajax = (url, method = 'GET', data = null, headers) => {
    return fetch(PATH_URL + url, {
        method,
        body: data ? JSON.stringify(data) : null,
        headers: {
            'Content-Type': 'application/json',
            ...headers
        }
    });
};

