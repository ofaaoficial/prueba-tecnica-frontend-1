import React, {useState} from 'react';
import {ajax} from '../helpers/fetchAPI';
import {Link, useHistory} from 'react-router-dom';
import SweetAlert from 'sweetalert2-react';


const Register = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [show, setShow] = useState(false);
    const history = useHistory();

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!email && !password) {
            return;
        }

        ajax('/api/login', 'POST', {email, password})
            .then(async (response) => {
                const {token, username} = await response.json();
                if (response.status === 200 && token && username) {
                    localStorage.token = token;
                    localStorage.username = username;
                    return history.push('/noticias');
                } else {
                    setShow(true);
                }
            })
            .catch(console.log);
    }

    return (
        <>
            <SweetAlert
                show={show}
                title="Incorrecto"
                text="Datos de acceso incorrectos."
                onConfirm={() => setShow(false)}
            />
            <section className="container d-flex justify-content-center align-items-center" style={{height: '100vh'}}>
                <article className="row w-100">
                    <form className="col-md-6 offset-md-3 form-styles" onSubmit={handleSubmit}>
                        <h1 className="form-title">Ingresar</h1>
                        <section className="form-group">
                            <label htmlFor="email">Correo electrónico</label>
                            <input type="email" className="form-control" id="email" aria-describedby="email"
                                   required
                                   value={email}
                                   onChange={({target: {value}}) => setEmail(value)}
                            />
                        </section>
                        <section className="form-group">
                            <label htmlFor="password">Contraseña</label>
                            <input type="password" className="form-control" id="password"
                                   required
                                   value={password}
                                   onChange={({target: {value}}) => setPassword(value)}
                            />
                        </section>
                        <button type="submit" className="btn btn-primary">Ingresar</button>
                        <Link className="link" to="/registro">Registrarme</Link>
                    </form>
                </article>
            </section>
        </>
    );
}

export default Register;
