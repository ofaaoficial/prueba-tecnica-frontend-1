import React, {useEffect, useState} from 'react';
import {ajax} from "../helpers/fetchAPI";
import Menu from "../components/Menu";
import Post from "../components/PostComponent";
import PostCreateComponent from "../components/PostCreateComponent";

const Posts = () => {
    const [posts, setPosts] = useState([]);
    const [search, setSearch] = useState([]);

    const handleCreate = (post) => setPosts([post, ...posts]);
    const handleDelete = (id) => {
        setPosts(posts.filter(({_id}) => _id !== id))
        setSearch(search.filter(({_id}) => _id !== id))
    };
    const handleSearch = (post) => setSearch(post);

    useEffect(() => {
        const abortController = new AbortController();
        const fetchData = async () => {
            const response = await ajax('/api/post', 'GET', null, {
                'x-access-token': localStorage.token
            });

            const posts = await response.json();
            setPosts(posts);
        }
        fetchData();

        return () => abortController.abort();
    }, []);

    return (
        <>
            <Menu onSearch={handleSearch}/>
            <section className="container">
                <h1 className="title-principal">Noticias</h1>

                <PostCreateComponent onCreate={handleCreate}/>
                {search.length > 0 &&
                <article className="posts">
                    <h2>Registros encontrados:</h2>
                    {search.map(({title, content, image_url, _id}, index) =>
                        <Post title={title}
                              content={content}
                              image_url={image_url}
                              _id={_id}
                              onDelete={handleDelete}
                              key={index}/>
                    )}
                    <p className="end-results">Fin de resultados</p>
                </article>}
                <article className="posts">
                    {posts.length > 0 ? posts.map(({title, content, image_url, _id}, index) =>
                        <Post title={title}
                              content={content}
                              image_url={image_url}
                              _id={_id}
                              onDelete={handleDelete}
                              key={index}/>
                    ) : <h3>No tiene noticias registradas.</h3>}
                </article>
            </section>
        </>
    )
}

export default Posts;
