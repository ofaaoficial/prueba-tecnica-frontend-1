import React, {useState} from 'react';
import {useHistory} from "react-router-dom";
import {ajax} from "../helpers/fetchAPI";

const Menu = ({onSearch}) => {
    const history = useHistory();
    const [search, setSearch] = useState('');

    const exit = (e) => {
        e.preventDefault();
        localStorage.clear();
        history.push('/');
    }

    const searchPosts = async (e) => {
        e.preventDefault();
        const response = await ajax(`/api/post/${search}`, 'GET', null, {
            'x-access-token': localStorage.token
        })

        const data = await response.json();

        if(response.status === 200) {
            onSearch([data])
        }
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <section className="container">
                <h2 className="navbar-brand">Solut<span>!</span>on</h2>
                <button className="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                        <form className="form-inline my-2 my-lg-0" onSubmit={searchPosts}>
                            <input className="form-control mr-sm-2" type="search" placeholder="Filtrar noticias"
                                   value={search}
                                   onChange={({target: {value}}) => setSearch(value)}
                                   aria-label="Filtrar"/>
                            <button className="btn btn-outline-success my-2 my-sm-0"
                                    type="submit">Buscar</button>
                        </form>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" href="#menu" id="navbarDropdownMenuLink"
                               role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {localStorage.username}
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a className="dropdown-item"
                                   href='/'
                                   onClick={exit}
                                >Salir</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        </nav>
    );
}

export default Menu;
