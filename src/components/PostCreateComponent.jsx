import React, {useState} from 'react';
import {ajax} from '../helpers/fetchAPI';
import SweetAlert from 'sweetalert2-react';

const PostCreateComponent = ({onCreate}) => {
    const [title, setTitle] = useState('');
    const [alert, setAlert] = useState({show: false, title: '', text: ''});
    const [content, setContent] = useState('');
    const [image_url, setImageUrl] = useState('');

    const closeModal = () => {
        const $MODAL = document.querySelector('#exampleModal');
        const $MODAL_BACK_DROP = document.querySelector('.modal-backdrop');
        const $BODY = document.querySelector('body');

        $MODAL.classList.remove('show');
        $MODAL.style.display = 'none';
        $MODAL_BACK_DROP.remove();
        $BODY.classList.toggle('modal-open')
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        if (title && content && image_url) {
            ajax('/api/post', 'POST', {title, content, image_url}, {
                'x-access-token': localStorage.token
            })
                .then(async (response) => {
                    const {post} = await response.json();
                    if (response.status === 200) {
                        setAlert({show: true, title: 'Bien!', text: 'La noticia se creo correctamente.'});
                        setTitle('');
                        setContent('');
                        setImageUrl('');
                        onCreate(post);
                    } else {
                        setAlert({show: true, title: 'Advertencia!', text: 'Hay un problema al crear la noticia.'});
                    }
                    closeModal();
                })
                .catch(console.log);
        }
    }

    return (
        <>
            <SweetAlert
                show={alert.show}
                title={alert.title}
                text={alert.text}
                onConfirm={() => setAlert({show: false})}
            />
            <button className="btn btn-outline-success mb-4"
                    data-toggle="modal"
                    data-target="#exampleModal">
                Registrar noticia
            </button>

            <section className="modal fade" id="exampleModal" tabIndex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                <article className="modal-dialog">
                    <section className="modal-content">
                        <article className="modal-header">
                            <h5 className="modal-title">Registrar noticia</h5>
                            <button type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </article>
                        <article className="modal-body">
                            <form onSubmit={handleSubmit}>
                                <section className="form-group">
                                    <label htmlFor="title">Titulo</label>
                                    <input type="text" className="form-control"
                                           id="title"
                                           aria-describedby="title"
                                           required
                                           value={title}
                                           onChange={({target: {value}}) => setTitle(value)}
                                    />
                                </section>
                                <section className="form-group">
                                    <label htmlFor="image_url">Imagen</label>
                                    <input type="url" className="form-control"
                                           id="image_url"
                                           aria-describedby="image_url"
                                           required
                                           value={image_url}
                                           onChange={({target: {value}}) => setImageUrl(value)}
                                    />
                                </section>
                                <section className="form-group">
                                    <label htmlFor="content">Contenido</label>
                                    <textarea name="content"
                                              id="content"
                                              cols="30" rows="5"
                                              className="form-control"
                                              required
                                              value={content}
                                              onChange={({target: {value}}) => setContent(value)}
                                    ></textarea>
                                </section>
                                <button type="submit" className="btn btn-primary">Registrar</button>
                            </form>
                        </article>
                    </section>
                </article>
            </section>
        </>
    )
};

export default PostCreateComponent;
