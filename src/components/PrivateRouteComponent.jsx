import {Redirect, Route} from 'react-router-dom';
import React from 'react';

const PrivateRouteComponent = ({children, ...rest}) => {
    return (
        <Route
            {...rest}
            render={({location}) =>
                localStorage.token ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: "/",
                            state: {from: location}
                        }}
                    />
                )
            }
        />
    );
}

export default PrivateRouteComponent;
