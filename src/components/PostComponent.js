import React from "react";
import {ajax} from "../helpers/fetchAPI";

const Post = ({title, image_url, content, _id, onDelete}) => {
    const destroy = async () => {
        const response = await ajax(`/api/post/${_id}`, 'DELETE', null, {
            'x-access-token': localStorage.token
        });

        if(response.status === 200) {
            onDelete(_id);
        }
    }

    return (
        <section className="post">
            <span className="delete" onClick={() => destroy(_id)}>&times;</span>
            <h2 className="post-title">{title}</h2>
            <p className="post-content">{content}</p>
            <img className="post-image" src={image_url}  alt={title}/>
        </section>
    );
}

export default Post;
