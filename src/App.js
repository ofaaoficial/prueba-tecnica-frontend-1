import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Posts from "./pages/Posts";
import PrivateRouteComponent from "./components/PrivateRouteComponent";

const App = () => {
    return (
        <Router basename={process.env.PUBLIC_URL}>
            <Switch>
                <Route exact path="/">
                    <Login/>
                </Route>
                <Route path="/registro">
                    <Register/>
                </Route>
                <PrivateRouteComponent path="/noticias">
                    <Posts/>
                </PrivateRouteComponent>
            </Switch>
        </Router>
    );
}

export default App;
